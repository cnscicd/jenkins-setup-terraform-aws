terraform {
  backend "s3" {
    bucket = "cns-node-aws-jenkins-terraform"
    key    = "cns-jenkins.terraform.tfstate"
    region = "eu-west-1"
  }
}

