# Internet VPC
resource "aws_vpc" "cns-jenkins" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "cns-jenkins"
  }
}

# Subnets
resource "aws_subnet" "cns-jenkins-public-1" {
  vpc_id                  = aws_vpc.cns-jenkins.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "eu-west-1a"

  tags = {
    Name = "cns-jenkins-public-1"
  }
}

# Internet GW
resource "aws_internet_gateway" "cns-jenkins-gw" {
  vpc_id = aws_vpc.cns-jenkins.id

  tags = {
    Name = "cns-jenkins"
  }
}

# route tables
resource "aws_route_table" "cns-jenkins-public" {
  vpc_id = aws_vpc.cns-jenkins.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cns-jenkins-gw.id
  }

  tags = {
    Name = "cns-jenkins-public-1"
  }
}

# route associations public
resource "aws_route_table_association" "cns-jenkins-public-1-a" {
  subnet_id      = aws_subnet.cns-jenkins-public-1.id
  route_table_id = aws_route_table.cns-jenkins-public.id
}

