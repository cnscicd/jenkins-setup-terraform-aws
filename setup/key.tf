resource "aws_key_pair" "cns-keypair" {
  key_name   = "cns-keypair"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
  lifecycle {
    ignore_changes = [public_key]
  }
}

